# README #

## Log-Parsing Library ##

The reason I started this project was first a way to parse my Squid3 logs into something readable by my non-technical wife. Then I started thinking that I could use this for other logs besides Squid3. For example on Windows systems I like to use LogExpert for displaying log information, but I've not found software for Linux that is comparable. So I would like to see where I can take this little parser. 

For full maven generated documentation you can find [here](http://jrsofty1.stinkbugonline.com/projects/Log-Parser/1.0.1/).

### Licensing ###

This project uses the MIT open source license. You will find it in the source code in both markdown and plain text file.

### What is this repository for? ###

* This library is for the parsing of various text based log files, and handling them in an object oriented fashion.
* Current Release version: 1.0.1 

### Get the Jars! ###

If you are using Maven.

```xml

<dependency>
  <groupId>org.bitbucket.jrsofty</groupId>
  <artifactId>log-parser</artifactId>
  <version>1.0.1</version>
</dependency>

```


### Usage of Library ###

* Information on usage is located in the [wiki](https://bitbucket.org/jrsofty/log-parsing-library/wiki/Home).

### How do I get set up? ###

* Java JRE 1.7+ (Java 7)
* Maven 3

### Contribution guidelines ###

* None yet will add some in the future.

### Who do I talk to? ###

* Contact me here or directly via [email](mailto:jrsofty@gmail.com).
