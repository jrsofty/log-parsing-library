/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A container class for holding the regex pattern and token marker used for mapping the token.
 *
 * @author jrsofty
 *
 */
public class TokenMatcher {

  private final Pattern regexPattern;
  private final String tokenMarker;

  public TokenMatcher(final String regex, final String tokenMarker) {
    this.tokenMarker = tokenMarker;
    this.regexPattern = Pattern.compile(regex);
  }

  /**
   * The regex pattern that was used for creating the regex used.
   * 
   * @return the regex pattern as String.
   */
  public String getPattern() {
    return this.regexPattern.toString();
  }

  /**
   * Delivers the token marker used for mapping. Token markers are not always exactly as they were
   * entered in the formatting string. Every token marker will be extended with an instance number.
   * 
   * @return this TokenMatcher's token marker.
   */
  public String getTokenMarker() {
    return this.tokenMarker;
  }

  /**
   * Validates the given token with the compiled regex pattern. Returns null if there is not a
   * match.
   *
   * @param token
   *          a portion of the log line as String
   * @return the token value if a match, otherwise returns null.
   * @throws TokenParseException
   *           when a match fails.
   */
  public String validateToken(final String token) throws TokenParseException {

    final Matcher matcher = this.regexPattern.matcher(token);
    matcher.find();
    try {
      final String result = matcher.group(0);
      return result;
    } catch (final IllegalStateException e) {
      throw new TokenParseException(token, this.tokenMarker, this.regexPattern.toString(), e);
    }

  }

}
