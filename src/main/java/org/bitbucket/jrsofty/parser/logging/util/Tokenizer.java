/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class providing multiple ways to tokenize a String for parsing.
 *
 * @author jrsofty
 *
 */
public class Tokenizer {
  /**
   * Splits a given string into tokens based on white space. This ignores quotation marks. So in a
   * line like
   *
   * <pre>
   * The "dirty red" fish was here.
   * </pre>
   *
   * <p>
   * the tokens would be
   * </p>
   * <ul>
   * <li>The</li>
   * <li>"dirty</li>
   * <li>red"</li>
   * <li>fish</li>
   * <li>was</li>
   * <li>here.</li>
   * </ul>
   *
   *
   * @param value
   *          The String to be tokenized.
   * @return an array of String containing the tokens of the value.
   */
  public String[] simpleWhiteSpaceTokenizer(final String value) {
    // final String[] tokens = value.split("(\\S+)");
    if (null == value) {
      return new String[] {};
    }
    final ArrayList<String> tokenList = new ArrayList<String>();
    final Matcher regexMatcher = Pattern.compile("(\\S+)").matcher(value);
    while (regexMatcher.find()) {

      tokenList.add(regexMatcher.group(1));

    }

    return tokenList.toArray(new String[tokenList.size()]);
  }

  /**
   * Splits a given string into tokens based on white space. This function recognizes quotation
   * marks and any white space there contained is kept. So in a line like
   *
   * <pre>
   * The "dirty red" fish was here.
   * </pre>
   * <p>
   * the tokens would be
   * </p>
   * <ul>
   * <li>The</li>
   * <li>dirty red</li>
   * <li>fish</li>
   * <li>was</li>
   * <li>here.</li>
   * </ul>
   *
   * @param value
   *          the text line that should be split into tokens.
   * @return an array of String containing the tokens of the value.
   */
  public String[] quotedWhiteSpaceTokenizer(final String value) {
    final ArrayList<String> tokenList = new ArrayList<String>();
    final String regex = "\"([^\"]*)\"|(\\S+)";
    final Matcher regexMatcher = Pattern.compile(regex).matcher(value);
    while (regexMatcher.find()) {
      if (regexMatcher.group(1) != null) {
        tokenList.add(regexMatcher.group(1));
      } else {
        tokenList.add(regexMatcher.group(2));
      }
    }

    return tokenList.toArray(new String[tokenList.size()]);
  }

  /**
   * Split the value string with a custom regex pattern. This method supports multiple groups in the
   * regex pattern.
   *
   * @param regexPattern
   *          The RegEx pattern as String
   * @param value
   *          the value to be split by the RegEx pattern.
   * @return an array of String containing the tokens of the value.
   */
  public String[] tokenizeWithPattern(final String regexPattern, final String value) {
    final ArrayList<String> tokenList = new ArrayList<String>();
    final Matcher regexMatcher = Pattern.compile(regexPattern).matcher(value);
    while (regexMatcher.find()) {
      for (int i = 1; i <= regexMatcher.groupCount(); i++) {
        if (regexMatcher.group(i) != null) {
          tokenList.add(regexMatcher.group(i));
        }
      }
    }

    return tokenList.toArray(new String[tokenList.size()]);
  }
}
