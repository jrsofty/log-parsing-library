/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

/**
 * This class is thrown when an exception happens while attempting to make an instance of the class
 * and an exception is thrown.
 *
 * @author jrsofty
 *
 */
public class IllegalClassException extends Exception {

  private static final long serialVersionUID = -6836983566924418028L;

  public IllegalClassException() {
    super("The passed value is not valid and is most likely null");
  }

  public IllegalClassException(final Class<?> clazz, final Throwable t) {
    super("The class " + clazz.getName() + " could not be accessed by reflection.", t);
  }

  public IllegalClassException(final Class<?> clazz, final String message) {
    super("The class " + clazz.getName() + " failed because " + message);
  }

}
