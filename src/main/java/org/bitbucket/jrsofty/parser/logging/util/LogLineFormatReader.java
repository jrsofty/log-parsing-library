/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.util;

import java.util.HashMap;
import java.util.Stack;

import org.bitbucket.jrsofty.parser.logging.api.TokenMatcher;

public class LogLineFormatReader {

  private static final String VALID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  private static final int CMD_LEN = 3;
  private static final int DATE_FORMAT_LEN_1 = 1;
  private static final int DATE_FORMAT_LEN_2 = 2;
  private static final int DATE_FORMAT_LEN_3 = 3;
  private static final int DATE_FORMAT_LEN_4 = 4;

  private final HashMap<String, Integer> formatTokenInstanceCounter = new HashMap<String, Integer>();

  /**
   * Creates a hash map containing the token matchers for each token in the order in which they
   * should be expected in the log line.
   *
   * @param format
   *          a String containing the log line format expected.
   * @return a HashMap&lt;Integer, TokenMatcher&gt; containing the token matching class in the
   *         expected order.
   * @throws LogLineFormatException
   *           when the given format is invalid.
   */
  public HashMap<Integer, TokenMatcher> createTokenMatchers(final String format)
      throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> map = new HashMap<Integer, TokenMatcher>();
    int formatIndex = 0;
    final String trimmedInput = format.trim() + " ";
    final StringBuffer regexBuffer = new StringBuffer();
    final Stack<Character> wrapperStack = new Stack<Character>();
    final String openWrapper = "[({\"'`";
    final String closeWrapper = "])}\"'`";
    String lastCommand = "";
    for (int i = 0; i < trimmedInput.length();) {
      final char c = trimmedInput.charAt(i);
      if (c == '%') {
        i++;
        final String cmd = trimmedInput.substring(i, i + LogLineFormatReader.CMD_LEN);
        i += LogLineFormatReader.CMD_LEN;
        if (cmd.equals("dtm")) {
          final StringBuffer dtmFormatBuffer = new StringBuffer();
          char ca = trimmedInput.charAt(i);
          if (ca != '{') {
            throw new LogLineFormatException("Expected '{' token not found at " + i);
          }
          i++;
          while ((ca = trimmedInput.charAt(i)) != '}') {
            dtmFormatBuffer.append(ca);
            i++;
            if (i >= trimmedInput.length()) {
              throw new LogLineFormatException("Expected '}' token not found by " + i);
            }
          }
          if (dtmFormatBuffer.toString().isEmpty()) {
            throw new LogLineFormatException(
                "Invalid date time formatting. There must be a formatting for date and time provided.");
          }
          regexBuffer.append(this.createRegExForDateTime(dtmFormatBuffer.toString()));
          i++;
        } else if (cmd.equals("ip4")) {
          regexBuffer.append(this.getRegExForIp4Address());
        } else if (cmd.equals("url")) {
          regexBuffer.append(this.getRegExForUrlMatch());
        } else if (cmd.equals("str")) {
          regexBuffer.append(this.getRegExForSimpleString());
        } else if (cmd.equals("opt")) {
          char ca = trimmedInput.charAt(i);
          if (ca != '{') {
            throw new LogLineFormatException("Expected '{' token not found at " + i);
          }
          i++;
          final StringBuffer wordBuffer = new StringBuffer();
          final StringBuffer listBuffer = new StringBuffer();

          while ((ca = trimmedInput.charAt(i)) != '}') {

            if (ca == ',') {

              listBuffer.append(wordBuffer.toString());
              wordBuffer.delete(0, wordBuffer.length());
              listBuffer.append('|');
            } else {
              final String strChar = String.valueOf(ca);
              if (!LogLineFormatReader.VALID_CHARS.contains(strChar)) {
                final String caStr = String.valueOf(ca);
                throw new LogLineFormatException("'" + caStr + "' is an invalid character.");
              }
              wordBuffer.append(ca);
            }

            i++;

          }
          // Wraps up the last text elements.
          listBuffer.append(wordBuffer.toString());

          regexBuffer.append('(' + listBuffer.toString() + ')');
          i++;
        } else if (cmd.equals("msg")) {
          regexBuffer.append(this.getRegExForMessage());
        } else if (cmd.equals("int")) {
          regexBuffer.append(this.getRegExForIntegerMatch());

        } else {
          throw new LogLineFormatException(
              "The command " + cmd + " is an unsupported format token");
        }
        lastCommand = cmd;
      } else if (c == ' ') {
        map.put(formatIndex, new TokenMatcher(regexBuffer.toString(),
            '%' + this.createMappingInstanceNumbering(lastCommand)));
        formatIndex++;
        regexBuffer.delete(0, regexBuffer.length());
        i++;
      } else {
        final String test = String.valueOf(c);

        if (openWrapper.contains(test)) {
          wrapperStack.push(c);
        } else if (closeWrapper.contains(test)) {
          final int ndx = closeWrapper.indexOf(test);
          final char closeC = openWrapper.charAt(ndx);
          final Character ca = wrapperStack.pop();
          if (ca.charValue() != closeC) {
            throw new LogLineFormatException(
                "Unclosed wrapper. Expecting " + closeC + " found " + ca.charValue());
          } else {
            // do nothing.
          }
        }
        regexBuffer.append(this.escapeCharacter(c));
        i++;
      }
    }

    return map;

  }

  private String createMappingInstanceNumbering(final String commandToken) {
    String result = null;
    if (this.formatTokenInstanceCounter.containsKey(commandToken)) {
      Integer value = this.formatTokenInstanceCounter.get(commandToken);
      value = value + 1;
      result = commandToken + "[" + value + "]";
      this.formatTokenInstanceCounter.put(commandToken, value);
    } else {
      this.formatTokenInstanceCounter.put(commandToken, 0);
      result = commandToken + "[0]";
    }
    return result;
  }

  private String createRegExForDateTime(final String formatInfo) throws LogLineFormatException {

    final String paddedInfo = formatInfo + " ";
    final StringBuffer buffer = new StringBuffer("(");
    boolean hasDateElement = false;
    char lastChar = ' ';
    int count = 0;
    for (int i = 0; i < paddedInfo.length(); i++) {
      final char c = paddedInfo.charAt(i);
      if (!String.valueOf(c).matches("[yuYdwhHmsZM]")) {
        if (count > 0) {
          buffer.append(this.getRegExForDateTimeElement(lastChar, count));
          hasDateElement = true;
          count = 0;
        }

        buffer.append(this.escapeCharacter(c));
        lastChar = c;
      } else {

        if ((count > 0) && (c != lastChar)) {
          buffer.append(this.getRegExForDateTimeElement(lastChar, count));
          hasDateElement = true;
          count = 0;
        }

        lastChar = c;
        count++;
      }

    }
    if (!hasDateElement) {
      throw new LogLineFormatException("The date format did not contain any date elements.");
    }

    final String output = buffer.toString().trim() + ")?";
    return output;
  }

  private String escapeCharacter(final char c) {
    final String reservedChars = "^!=$?*+\\[].(){}|";
    final String testChar = String.valueOf(c);

    if (reservedChars.contains(testChar)) {
      return "\\" + testChar;
    }
    return testChar;

  }

  private String getRegExForIntegerMatch() {
    return "([\\d]+)?";
  }

  private String getRegExForUrlMatch() {
    return "(((http[s]*://)([\\S]+))|([-]))?";
  }

  private String getRegExForSimpleString() {
    return "([\\S]+)?";
  }

  private String getRegExForMessage() {
    return "([\\s\\S]+)?";
  }

  private String getRegExForIp4Address() {
    return "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))?";
  }

  private String getRegExForDateTimeElement(final char c, final int count)
      throws LogLineFormatException {
    String output = null;
    if ((c == 'y') || (c == 'u') || (c == 'Y')) {
      if ((count != LogLineFormatReader.DATE_FORMAT_LEN_2)
          && (count != LogLineFormatReader.DATE_FORMAT_LEN_4)) {
        throw new LogLineFormatException("Year formats must have two or four characters.");
      }
      output = "([0-9]{" + count + "})";
    } else if ((c == 'd')) {
      if (count == LogLineFormatReader.DATE_FORMAT_LEN_1) {
        output = "([0-9]{1,2})";
      } else if (count == 3) {
        output = "([0-9]{3})";
      } else if (count == 2) {
        output = "([0-9]{2})";
      } else {
        throw new LogLineFormatException("Day formatting is either 'd' or 'dd' or 'ddd'.");
      }
    } else if ((c == 'w') || (c == 'h') || (c == 'H') || (c == 'm') || (c == 's')) {
      if ((count != LogLineFormatReader.DATE_FORMAT_LEN_2)) {
        throw new LogLineFormatException(
            "Week, hour, minute, second elements require 2 characters");
      }
      output = "([0-9]{2})";
    } else if (c == 'Z') {
      if (count != LogLineFormatReader.DATE_FORMAT_LEN_1) {
        throw new LogLineFormatException("No more than one time zone character");
      }
      output = "([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2})";
    } else {
      if (count == LogLineFormatReader.DATE_FORMAT_LEN_1) {
        output = "([0-9]{1,2})";
      } else if (count == LogLineFormatReader.DATE_FORMAT_LEN_2) {
        output = "([0-9]{2})";
      } else if (count == LogLineFormatReader.DATE_FORMAT_LEN_3) {
        output = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
      } else {
        output = "(January|February|March|April|May|June|July|August|September|October|November|December)";
      }
    }

    return output;
  }

}
