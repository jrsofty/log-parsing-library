/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;

import org.bitbucket.jrsofty.parser.logging.api.IllegalClassException;
import org.bitbucket.jrsofty.parser.logging.api.LogElementMapping;

public class ClassFieldMapper {
  private final HashMap<String, Integer> instanceNumberingMap = new HashMap<String, Integer>();

  /**
   * The method uses reflection to find all fields that are mapped with the
   * {@link LogElementMapping} annotation then provides a HashMap mapping the field with the
   * formatting token.
   *
   * @param clazz
   *          the class.
   * @return HashMap&lt;String, String&gt; containing the fields mapped to the formatting tokens.
   * @throws IllegalClassException
   *           when the class contains no fields annotated with LogElementMapping.
   */
  public HashMap<String, String> mapClassFields(final Class<?> clazz) throws IllegalClassException {
    if (null == clazz) {
      throw new IllegalClassException();
    }
    final HashMap<String, String> map = new HashMap<String, String>();
    final Field[] fields = clazz.getDeclaredFields();

    for (final Field field : fields) {
      final Annotation[] annotations = field.getAnnotations();
      for (final Annotation annotation : annotations) {
        if (annotation instanceof LogElementMapping) {
          final String tokenName = this
              .convertToInstanceNumbering(((LogElementMapping) annotation).logToken());
          map.put(tokenName, field.getName());
        }
      }
    }

    if (map.size() == 0) {
      throw new IllegalClassException(
          clazz.getClass(), " does not contain @LogElementMapping on fields.");
    }

    return map;
  }

  private String convertToInstanceNumbering(final String token) {
    String result = null;
    if (this.instanceNumberingMap.containsKey(token)) {
      Integer value = this.instanceNumberingMap.get(token);
      value = value + 1;
      result = token + "[" + value + "]";
      this.instanceNumberingMap.put(token, value);
    } else {
      this.instanceNumberingMap.put(token, 0);
      result = token + "[0]";
    }

    return result;
  }
}
