/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatException;

/**
 * Interface for creating LogParserFactory implementations.
 *
 * @author jrsofty
 *
 */
public interface LogParserFactory {

  /**
   * Provides the default parser factory.
   *
   * @return the DefaultLogParserFactory implementation of this interface.
   */
  public static LogParserFactory createDefaultInstance() {
    return new DefaultLogParserFactory();

  }

  /**
   * Creates an instance of the LogParser Implementation.
   *
   * @param format
   *          a String with the formatting tokens for describing the expected log line format to
   *          match.
   * @param clazz
   *          a class that has implemented the LogEntry interface and has fields mapped with the
   *          LogElementMapping annotations matching the tokens in the format param.
   * @return An instance of the LogParser.
   * @throws LogLineFormatException
   *           when the formatting tokens are invalid or cannot be used.
   * @throws IllegalClassException
   *           when the class object cannot be instanced, or reflected, or when the
   *           LogElementMapping annotations are missing.
   */
  LogParser createParserForFormat(String format, Class<?> clazz)
      throws LogLineFormatException, IllegalClassException;

  class DefaultLogParserFactory implements LogParserFactory {

    @Override
    public LogParser createParserForFormat(final String format,
        final Class<?> clazz)
        throws LogLineFormatException, IllegalClassException {
      final DefaultLogParser defaultParser = new DefaultLogParser(format, clazz);
      return defaultParser;
    }

  }

}
