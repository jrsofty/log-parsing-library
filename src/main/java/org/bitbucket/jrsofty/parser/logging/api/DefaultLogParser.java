/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.bitbucket.jrsofty.parser.logging.util.ClassFieldMapper;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatException;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatReader;
import org.bitbucket.jrsofty.parser.logging.util.Tokenizer;

/**
 * The default parser for those who want to use the most basic parser.
 *
 * @author jrsofty
 *
 */
public class DefaultLogParser implements LogParser {

  private HashMap<Integer, TokenMatcher> matchers = null;
  private HashMap<String, String> fieldMapping = new HashMap<String, String>();
  private Class<?> logEntryClass = null;
  private final String logFormatString;
  private final Tokenizer tokenizer = new Tokenizer();
  private final String tokenizerPattern = "\\[([^\\]]*)\\]|\"([^\"]*)\"|(\\S+)";

  protected DefaultLogParser(final String logFormatString, final Class<?> entryClass)
      throws LogLineFormatException, IllegalClassException {
    this.logFormatString = logFormatString;
    this.initializeMatchers(this.logFormatString);
    this.logEntryClass = entryClass;
    this.matchClassWithLogFormat();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LogEntry parseLogString(final String logString)
      throws TokenParseException, IllegalClassException {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(this.tokenizerPattern, logString);
    final LogEntry instance = this.createLogEntryInstance();
    for (int i = 0; i < tokens.length; i++) {
      final TokenMatcher matcher = this.matchers.get(i);
      final String result = matcher.validateToken(tokens[i]);
      if (!result.equals(tokens[i])) {
        throw new TokenParseException(tokens[i], matcher.getTokenMarker(), matcher.getPattern());
      }

      this.setValueInLogEntry(instance, this.fieldMapping.get(matcher.getTokenMarker()), result);

    }

    return instance;
  }

  private LogEntry setValueInLogEntry(final LogEntry instance, final String fieldName,
      final String value) throws IllegalClassException {
    final String methodName = "set" + fieldName.substring(0, 1).toUpperCase()
        + fieldName.substring(1);

    try {
      final Method method = instance.getClass().getDeclaredMethod(methodName, String.class);
      method.invoke(instance, value);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      throw new IllegalClassException(instance.getClass(), e);
    }

    return instance;
  }

  private LogEntry createLogEntryInstance() throws IllegalClassException {
    Object clazz;
    try {
      clazz = this.logEntryClass.getConstructor().newInstance();
      if (!(clazz instanceof LogEntry)) {
        throw new IllegalClassException(this.logEntryClass.getClass(),
            "Does not implement the LogEntry interface.");
      }
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException | NoSuchMethodException | SecurityException e) {
      throw new IllegalClassException(this.logEntryClass.getClass(), e);
    }

    return (LogEntry) clazz;

  }

  private void initializeMatchers(final String logFormatString) throws LogLineFormatException {
    if (null == logFormatString) {
      throw new LogLineFormatException("The log format string must not be null.");
    }
    if (logFormatString.isEmpty()) {
      throw new LogLineFormatException("The log format string must not be empty.");
    }
    final LogLineFormatReader reader = new LogLineFormatReader();
    this.matchers = reader.createTokenMatchers(logFormatString);
  }

  private void matchClassWithLogFormat() throws IllegalClassException {
    final ClassFieldMapper mapper = new ClassFieldMapper();
    this.fieldMapping = mapper.mapClassFields(this.logEntryClass);

    this.createLogEntryInstance();

    for (final TokenMatcher matcher : this.matchers.values()) {
      if (!this.fieldMapping.containsKey(matcher.getTokenMarker())) {
        throw new IllegalClassException(this.logEntryClass.getClass(),
            "Log format uses " + matcher.getTokenMarker()
                + " but it is not marked in the class.");
      }
    }
  }

}
