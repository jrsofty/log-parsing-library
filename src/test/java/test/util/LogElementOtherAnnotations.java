/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package test.util;

import org.bitbucket.jrsofty.parser.logging.api.LogElementMapping;
import org.bitbucket.jrsofty.parser.logging.api.LogEntry;

public class LogElementOtherAnnotations implements LogEntry {

  @LogElementMapping(logToken = "%ip4")
  private String ip4Value;

  @LogElementMapping(logToken = "%dtm")
  @Deprecated
  private String dateStringValue;

  public void setIp4Value(final String value1) {
    this.ip4Value = value1;
  }

  public void setDateStringValue(final String value2) {
    this.dateStringValue = value2;
  }

  public String getIp4Value() {
    return this.ip4Value;
  }

  public String getDateStringValue() {
    return this.dateStringValue;
  }

}
