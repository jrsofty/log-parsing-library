/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package test.util;

import org.bitbucket.jrsofty.parser.logging.api.LogElementMapping;
import org.bitbucket.jrsofty.parser.logging.api.LogEntry;

public class Squid3TestElement implements LogEntry {

  @LogElementMapping(logToken = "%ip4")
  private String ip;
  @LogElementMapping(logToken = "%dtm")
  private String dateString;
  @LogElementMapping(logToken = "%opt")
  private String method;
  @LogElementMapping(logToken = "%url")
  private String requestedUrl;
  @LogElementMapping(logToken = "%str")
  private String httpType;
  @LogElementMapping(logToken = "%int")
  private String status;
  @LogElementMapping(logToken = "%int")
  private String dataSize;
  @LogElementMapping(logToken = "%url")
  private String referringUrl;
  @LogElementMapping(logToken = "%msg")
  private String userAgent;
  @LogElementMapping(logToken = "%str")
  private String value1;

  public String getIp() {
    return this.ip;
  }

  public void setIp(final String ip) {
    this.ip = ip;
  }

  public String getDateString() {
    return this.dateString;
  }

  public void setDateString(final String dateString) {
    this.dateString = dateString;
  }

  public String getMethod() {
    return this.method;
  }

  public void setMethod(final String method) {
    this.method = method;
  }

  public String getRequestedUrl() {
    return this.requestedUrl;
  }

  public void setRequestedUrl(final String requestedUrl) {
    this.requestedUrl = requestedUrl;
  }

  public String getHttpType() {
    return this.httpType;
  }

  public void setHttpType(final String httpType) {
    this.httpType = httpType;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public String getDataSize() {
    return this.dataSize;
  }

  public void setDataSize(final String dataSize) {
    this.dataSize = dataSize;
  }

  public String getReferringUrl() {
    return this.referringUrl;
  }

  public void setReferringUrl(final String referringUrl) {
    this.referringUrl = referringUrl;
  }

  public String getUserAgent() {
    return this.userAgent;
  }

  public void setUserAgent(final String userAgent) {
    this.userAgent = userAgent;
  }

  public String getValue1() {
    return this.value1;
  }

  public void setValue1(final String value1) {
    this.value1 = value1;
  }

}
