/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package test.util;

import org.bitbucket.jrsofty.parser.logging.api.LogElementMapping;
import org.bitbucket.jrsofty.parser.logging.api.LogEntry;

public class LogElementDupAnno implements LogEntry {

  @LogElementMapping(logToken = "%url")
  private String url1;
  @LogElementMapping(logToken = "%url")
  private String url2;
  @LogElementMapping(logToken = "%ip4")
  private String ip4;

  public String getUrl1() {
    return this.url1;
  }

  public void setUrl1(final String url1) {
    this.url1 = url1;
  }

  public String getUrl2() {
    return this.url2;
  }

  public void setUrl2(final String url2) {
    this.url2 = url2;
  }

  public String getIp4() {
    return this.ip4;
  }

  public void setIp4(final String ip4) {
    this.ip4 = ip4;
  }

}
