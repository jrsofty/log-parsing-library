/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.util;

import java.util.HashMap;

import org.bitbucket.jrsofty.parser.logging.api.TokenMatcher;
import org.bitbucket.jrsofty.parser.logging.api.TokenParseException;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatException;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LogLineFormatReaderTest {

  // private final String defaultFormat = "%dtm{dd/MMM/yyyy:HH:mm:ss Z} %str %msg %exc";
  //

  private final String ip4Pattern = "%ip4";
  private final String ip4Result = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))?";
  private final String ip4GoodTest = "192.168.172.1";
  private final String ip4BadTest1 = "999.999.999.999";
  private final String ip4BadTest2 = "256.131.031.122";
  private final String ip4BadTest3 = "127.256.1.1";
  private final String ip4BadTest4 = "172.186.291.1";
  private final String ip4BadTest5 = "192.168.176.256";

  private final String integerPattern = "%int";
  private final String intGoodTest1 = "1";
  private final String intGoodTest2 = "12";
  private final String intGoodTest3 = "1234";
  private final String intGoodTest4 = "12345678";
  private final String intGoodTest5 = "1234567890123456789";
  private final String intBadTest1 = "0.1";
  private final String intBadTest2 = "4F";
  private final String intBadTest3 = "100,300,001";
  private final String intBadTest4 = "A String";
  private final String intBadTest5 = "123 456";

  private final String dateFailPattern = "%dtm{dd/MM/yyyy:HH:mm:ss Z}";
  private final String dateFailResult = "(([0-9]{2})/([0-9]{2})/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) ([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateOnePattern = "%dtm{dd/MMM/yyyy:HH:mm:ss Z}";
  private final String dateOneResult = "(([0-9]{2})/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)/([0-9]{4}):([0-9]{2}):([0-9]{2}):([0-9]{2}) ([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateTwoPattern = "%dtm{dd-MM-yyyy HH:mm:ss Z}";
  private final String dateTwoResult = "(([0-9]{2})-([0-9]{2})-([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}) ([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateThreePattern = "%dtm{yyyy-MM-ddTHH:mm:ssZ}";
  private final String dateThreeResult = "(([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateFourPattern = "%dtm{yyyyMMddHHmmssZ}";
  private final String dateFourResult = "(([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateFivePattern = "%dtm{dd.MM.yyyy HH:mm:ss Z}";
  private final String dateFiveResult = "(([0-9]{2})\\.([0-9]{2})\\.([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}) ([0-9\\+-]{5}|[0-9\\+-]{3}:[0-9]{2}))?";

  private final String dateSixPattern = "%dtm{dd MMMM yyyy HH:mm:ss}";
  private final String dateSixResult = "(([0-9]{2}) (January|February|March|April|May|June|July|August|September|October|November|December) ([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}))?";
  private final String dateSevenPattern = "%dtm{dd MMM yy hh:mm:ss}";
  private final String dateSevenResult = "(([0-9]{2}) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}))?";

  private final String dateEightPattern = "%dtm{dd.M.yyyy}";
  private final String dateEightRegEx = "(([0-9]{2})\\.([0-9]{1,2})\\.([0-9]{4}))?";

  private final String dateFailPattern1 = "%dtm{dddd}";
  private final String dateFailPattern2 = "%dtm{y}";
  private final String dateFailPattern3 = "%dtm{u}";
  private final String dateFailPattern4 = "%dtm{Y}";
  private final String dateFailPattern5 = "%dtm{yyyyy}";
  private final String dateFailPattern6 = "%dtm{uuuuu}";
  private final String dateFailPattern7 = "%dtm{YYYYY}";

  private final String urlPattern = "%url";
  private final String urlRegEx = "(((http[s]*://)([\\S]+))|([-]))?";
  private final String urlResult1 = "http://www.domain.com";
  private final String urlResult2 = "http://www.domain.com/folder";
  private final String urlResult3 = "http://subdomain.domain.com";
  private final String urlResult4 = "http://subdomain.domain.com/folder";
  private final String urlResult5 = "http://www.domain.com/index.php?a=1&b=2";
  private final String urlResult6 = "https://www.domain.com";
  private final String urlResult7 = "https://www.domain.com/folder";
  private final String urlResult8 = "https://www.domain.com/index.php?a=1&b=2";
  private final String urlResult9 = "https://subdomain.domain.com";
  private final String urlFailResult1 = "gopher://nope.not.working";

  private final String strPattern = "%str";
  private final String strRegEx = "([\\S]+)?";
  private final String strResult1 = "Yes";
  private final String strResult2 = "YES";
  private final String strResult3 = "132";
  private final String strResult4 = "A4A";
  private final String strResult5 = "Supercalifragilisticexpialidocious";
  private final String strResultFail = "This Should Not Match";

  private final String optPatternOk1 = "%opt{YES,NO,ONE,TWO}";
  private final String optPatternOk2 = "%opt{yes,no,one,two}";
  private final String optRegEx1 = "(YES|NO|ONE|TWO)";
  private final String optRegEx2 = "(yes|no|one|two)";
  private final String optPatternNOK1 = "%opt";
  private final String optPatternNOK2 = "%opt{YES,NO,ONE,TWO";
  private final String optPatternNOK3 = "%opt{YES|NO|ONE|TWO}";
  // private final String optYesResult = "YES";
  // private final String optNoResult = "NO";
  // private final String optOneResult = "ONE";
  // private final String optTwoResult = "TWO";
  // private final String optYesLowerResult = "yes";
  // private final String optNoLowerResult = "no";
  // private final String optOneLowerResult = "one";
  // private final String optTwoLowerResult = "two";

  private final String msgPattern = "%msg";
  private final String msgRegEx = "([\\s\\S]+)?";

  private final String mixedPattern = "%ip4 [%url]";
  private final String mixedRegEx = "\\[" + this.urlRegEx + "\\]";

  private final String mixedPatternFail = "%ip4 [%url)";

  private final String duplicatePattern = "%url %ip4 %url";

  private LogLineFormatReader formatReader;

  @Before
  public void setup() {
    this.formatReader = new LogLineFormatReader();
  }

  @Test
  public void testImplementation() {
    Assert.assertNotNull(this.formatReader);
  }

  @Test
  public void testFailPattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.dateFailPattern);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(matcher.getPattern().equals(this.dateFailResult));
  }

  @Test
  public void testDuplicatePattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.duplicatePattern);
    Assert.assertTrue(matchers.size() == 3);
  }

  @Test(expected = LogLineFormatException.class)
  public void testMixedPatternFail() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.mixedPatternFail);
  }

  @Test
  public void testMixedPattern1() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.mixedPattern);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.ip4Result.equals(matcher.getPattern()));
  }

  @Test
  public void testMixedPattern2() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.mixedPattern);
    final TokenMatcher matcher = matchers.get(1);
    Assert.assertTrue(this.mixedRegEx.equals(matcher.getPattern()));
  }

  @Test
  public void testMsgPattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.msgPattern);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.msgRegEx.equals(matcher.getPattern()));
  }

  @Test
  public void testOptPatternOk1() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.optPatternOk1);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.optRegEx1.equals(matcher.getPattern()));
  }

  @Test
  public void testOptPatternOk2() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.optPatternOk2);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.optRegEx2.equals(matcher.getPattern()));
  }

  @Test(expected = LogLineFormatException.class)
  public void testOptPatternFail1() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.optPatternNOK1);
  }

  @Test(expected = LogLineFormatException.class)
  public void testOptPatternFail2() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.optPatternNOK2);
  }

  @Test(expected = LogLineFormatException.class)
  public void testOptPatternFail3() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.optPatternNOK3);
  }

  @Test
  public void testStrPattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.strRegEx.equals(matcher.getPattern()));
  }

  @Test
  public void testStrResult1() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResult1);
    Assert.assertTrue(this.strResult1.equals(result));
  }

  @Test
  public void testStrResult2() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResult2);
    Assert.assertTrue(this.strResult2.equals(result));
  }

  @Test
  public void testStrResul3() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResult3);
    Assert.assertTrue(this.strResult3.equals(result));
  }

  @Test
  public void testStrResult4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResult4);
    Assert.assertTrue(this.strResult4.equals(result));
  }

  @Test
  public void testStrResult5() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResult5);
    Assert.assertTrue(this.strResult5.equals(result));
  }

  @Test
  public void testStrResultFail() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.strPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.strResultFail);
    Assert.assertFalse(this.strResultFail.equals(result));
  }

  @Test
  public void testUrlPattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue(this.urlRegEx.equals(matcher.getPattern()));
  }

  @Test
  public void testUrlResult1() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult1);
    Assert.assertTrue(this.urlResult1.equals(result));
  }

  @Test
  public void testUrlResult2() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult2);
    Assert.assertTrue(this.urlResult2.equals(result));
  }

  @Test
  public void testUrlResult3() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult3);
    Assert.assertTrue(this.urlResult3.equals(result));
  }

  @Test
  public void testUrlResult4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult4);
    Assert.assertTrue(this.urlResult4.equals(result));
  }

  @Test
  public void testUrlResult5() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult5);
    Assert.assertTrue(this.urlResult5.equals(result));
  }

  @Test
  public void testUrlResult6() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult6);
    Assert.assertTrue(this.urlResult6.equals(result));
  }

  @Test
  public void testUrlResult7() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult7);
    Assert.assertTrue(this.urlResult7.equals(result));
  }

  @Test
  public void testUrlResult8() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult8);
    Assert.assertTrue(this.urlResult8.equals(result));
  }

  @Test
  public void testUrlResult9() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlResult9);
    Assert.assertTrue(this.urlResult9.equals(result));
  }

  @Test
  public void testUrlFail() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.urlPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.urlFailResult1);
    Assert.assertFalse(this.urlFailResult1.equals(result));
  }

  @Test
  public void testIntegerGood1() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intGoodTest1);
    Assert.assertTrue(this.intGoodTest1.equals(result));
  }

  @Test
  public void testIntegerGood2() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intGoodTest2);
    Assert.assertTrue(this.intGoodTest2.equals(result));
  }

  @Test
  public void testIntegerGood3() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intGoodTest3);
    Assert.assertTrue(this.intGoodTest3.equals(result));
  }

  @Test
  public void testIntegerGood4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intGoodTest4);
    Assert.assertTrue(this.intGoodTest4.equals(result));
  }

  @Test
  public void testIntegerGood5() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intGoodTest5);
    Assert.assertTrue(this.intGoodTest5.equals(result));
  }

  @Test
  public void testIntegerBad1() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intBadTest1);
    Assert.assertFalse(this.intBadTest1.equals(result));
  }

  @Test
  public void testIntegerBad2() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intBadTest2);
    Assert.assertFalse(this.intBadTest2.equals(result));
  }

  @Test
  public void testIntegerBad3() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intBadTest3);
    Assert.assertFalse(this.intBadTest3.equals(result));
  }

  @Test
  public void testIntegerBad4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intBadTest4);
    Assert.assertFalse(this.intBadTest4.equals(result));
  }

  @Test
  public void testIntegerBad5() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers(this.integerPattern);
    final TokenMatcher matcher = matchers.get(0);
    final String result = matcher.validateToken(this.intBadTest5);
    Assert.assertFalse(this.intBadTest5.equals(result));
  }

  @Test(expected = LogLineFormatException.class)
  public void testUnsupportedToken() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%cmd");
  }

  @Test
  public void testIp4Pattern() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    Assert.assertTrue(this.ip4Result.equals(matches.get(0).getPattern()));
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFailOne() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm");
    Assert.fail("This should have thrown an exception.");
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFailTwo() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader.createTokenMatchers("%dtm{}");
    Assert.fail(
        "This should have thrown an exception instead we get " + matchers.get(0).getPattern());
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFailThree() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm{dd-MM-yyyy");
    Assert.fail("Exception should have been thrown.");
  }

  @Test
  public void testDateTimePassOne() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateOnePattern);
    Assert.assertTrue(this.dateOneResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassTwo() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateTwoPattern);
    Assert.assertTrue(this.dateTwoResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassThree() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateThreePattern);
    Assert.assertTrue(this.dateThreeResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassFour() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateFourPattern);
    Assert.assertTrue(this.dateFourResult.equals(matches.get(0).getPattern()));

  }

  @Test
  public void testDateTimePassFive() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateFivePattern);
    Assert.assertTrue(this.dateFiveResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassSix() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateSixPattern);
    Assert.assertTrue(this.dateSixResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassSeven() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateSevenPattern);
    Assert.assertTrue(this.dateSevenResult.equals(matches.get(0).getPattern()));
  }

  @Test
  public void testDateTimePassEight() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.dateEightPattern);
    Assert.assertTrue(this.dateEightRegEx.equals(matches.get(0).getPattern()));
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail1() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern1);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail2() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern2);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail3() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern3);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail4() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern4);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail5() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern5);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail6() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern6);
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail7() throws LogLineFormatException {
    this.formatReader.createTokenMatchers(this.dateFailPattern7);
  }

  @Test
  public void testDateTimePassNine() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers("%dtm{d}");
    final TokenMatcher matcher = matchers.get(0);
    Assert.assertTrue("(([0-9]{1,2}))?".equals(matcher.getPattern()));
  }

  @Test
  public void testDateTimePassTen() throws LogLineFormatException {
    final HashMap<Integer, TokenMatcher> matchers = this.formatReader
        .createTokenMatchers("%dtm{ddd}");
    Assert.assertTrue("(([0-9]{3}))?".equals(matchers.get(0).getPattern()));
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail8() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm{w}");
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail10() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm{www}");
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail11() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm{ZZ}");
  }

  @Test(expected = LogLineFormatException.class)
  public void testDateTimeFail12() throws LogLineFormatException {
    this.formatReader.createTokenMatchers("%dtm{q}");
  }

  @Test
  public void testGoodIp4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4GoodTest);
    Assert.assertTrue(this.ip4GoodTest.equals(validateToken));

  }

  @Test
  public void testBadIp1() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4BadTest1);
    Assert.assertFalse(this.ip4BadTest1.equals(validateToken));
  }

  @Test
  public void testBadIp2() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4BadTest2);
    Assert.assertFalse(this.ip4BadTest2.equals(validateToken));
  }

  @Test
  public void testBadIp3() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4BadTest3);
    Assert.assertFalse(this.ip4BadTest3.equals(validateToken));
  }

  @Test
  public void testBadIp4() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4BadTest4);
    Assert.assertFalse(this.ip4BadTest4.equals(validateToken));
  }

  @Test
  public void testBadIp5() throws LogLineFormatException, TokenParseException {
    final HashMap<Integer, TokenMatcher> matches = this.formatReader
        .createTokenMatchers(this.ip4Pattern);
    final TokenMatcher matcher = matches.get(0);
    final String validateToken = matcher.validateToken(this.ip4BadTest5);
    Assert.assertFalse(this.ip4BadTest5.equals(validateToken));
  }

}
