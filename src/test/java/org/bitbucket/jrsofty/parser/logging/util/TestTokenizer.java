/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTokenizer {

  private static final String[] WHITE_SPACE_TOKENS1 = new String[] { "The", "quick", "brown", "fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] WHITE_SPACE_TOKENS2 = new String[] { "The", "quick", "\"brown",
      "fox\"", "jumps", "over", "the", "lazy", "dog." };

  private static final String[] WHITE_SPACE_TOKENS3 = new String[] { "The", "[quick", "brown",
      "fox]", "jumps", "over", "the", "\"lazy", "dog\"." };

  private static final String[] QUOTED_SPACE_TOKENS1 = new String[] { "The", "quick", "brown",
      "fox", "jumps", "over", "the", "lazy", "dog." };
  private static final String[] QUOTED_SPACE_TOKENS2 = new String[] { "The", "quick", "brown fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] QUOTED_SPACE_TOKENS3 = new String[] { "The", "[quick", "brown",
      "fox]", "jumps", "over", "the", "lazy dog", "." };

  private static final String[] PATTERN1_TOKENS1 = new String[] { "The", "quick", "brown", "fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN1_TOKENS2 = new String[] { "The", "quick", "\"brown",
      "fox\"", "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN1_TOKENS3 = new String[] { "The", "[quick", "brown", "fox]",
      "jumps", "over", "the", "\"lazy", "dog\"." };

  private static final String[] PATTERN2_TOKENS1 = new String[] { "The", "quick", "brown", "fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN2_TOKENS2 = new String[] { "The", "quick", "brown fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN2_TOKENS3 = new String[] { "The", "[quick", "brown", "fox]",
      "jumps", "over", "the", "lazy dog", "." };

  private static final String[] PATTERN3_TOKENS1 = new String[] { "The", "quick", "brown", "fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN3_TOKENS2 = new String[] { "The", "quick", "brown fox",
      "jumps", "over", "the", "lazy", "dog." };
  private static final String[] PATTERN3_TOKENS3 = new String[] { "The", "quick brown fox", "jumps",
      "over", "the", "lazy dog", "." };
  private static final String[] TOKEN_WITH_NO_WHITESPACE = new String[] {
      "TheQuickBrownFoxJumpsOverTheLazyDog" };
  private static final String[] EMPTY_ARRAY = new String[] {};

  private static final String TEST_VALUE1 = "The quick brown fox jumps over the lazy dog.";
  private static final String TEST_VALUE2 = "The quick \"brown fox\" jumps over the lazy dog.";
  private static final String TEST_VALUE3 = "The [quick brown fox] jumps over the \"lazy dog\".";
  private static final String TEST_VALUE4 = "TheQuickBrownFoxJumpsOverTheLazyDog";

  private static final String PATTERN1 = "(\\S+)";
  private static final String PATTERN2 = "\"([^\"]*)\"|(\\S+)";
  private static final String PATTERN3 = "\\[([^\\]]*)\\]|\"([^\"]*)\"|(\\S+)";

  public Tokenizer tokenizer;

  /**
   * Creation of tokenizer test.
   */
  @Before
  public void start() {
    if (this.tokenizer != null) {
      this.tokenizer = null;
    }
    this.tokenizer = new Tokenizer();
  }

  @Test
  public void testSimpleWhiteSpaceTokenizerWithNull() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(null);
    Assert.assertArrayEquals(TestTokenizer.EMPTY_ARRAY, tokens);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizerWithoutWhiteSpace() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE4);
    Assert.assertArrayEquals(TestTokenizer.TOKEN_WITH_NO_WHITESPACE, tokens);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_1() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE1);
    Assert.assertEquals(9, tokens.length);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_2() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE2);
    Assert.assertEquals(9, tokens.length);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_3() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE3);
    Assert.assertEquals(9, tokens.length);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_4() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE1);
    Assert.assertArrayEquals(TestTokenizer.WHITE_SPACE_TOKENS1, tokens);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_5() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE2);
    Assert.assertArrayEquals(TestTokenizer.WHITE_SPACE_TOKENS2, tokens);
  }

  @Test
  public void testSimpleWhiteSpaceTokenizer_6() {
    final String[] tokens = this.tokenizer.simpleWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE3);
    Assert.assertArrayEquals(TestTokenizer.WHITE_SPACE_TOKENS3, tokens);
  }

  @Test
  public void testQuotedWhiteSpaceTokenizer_1() {
    final String[] tokens = this.tokenizer.quotedWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE1);
    Assert.assertArrayEquals(TestTokenizer.QUOTED_SPACE_TOKENS1, tokens);
  }

  @Test
  public void testQuotedWhiteSpaceTokenizer_2() {
    final String[] tokens = this.tokenizer.quotedWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE2);
    Assert.assertArrayEquals(TestTokenizer.QUOTED_SPACE_TOKENS2, tokens);
  }

  @Test
  public void testQuotedWhiteSpaceTokenizer_3() {
    final String[] tokens = this.tokenizer.quotedWhiteSpaceTokenizer(TestTokenizer.TEST_VALUE3);
    Assert.assertArrayEquals(TestTokenizer.QUOTED_SPACE_TOKENS3, tokens);
  }

  @Test
  public void testTokenizeWithPattern_1_1() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN1,
        TestTokenizer.TEST_VALUE1);
    Assert.assertArrayEquals(TestTokenizer.PATTERN1_TOKENS1, tokens);
  }

  @Test
  public void testTokenizeWithPattern_1_2() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN1,
        TestTokenizer.TEST_VALUE2);
    Assert.assertArrayEquals(TestTokenizer.PATTERN1_TOKENS2, tokens);
  }

  @Test
  public void testTokenizeWithPattern_1_3() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN1,
        TestTokenizer.TEST_VALUE3);
    Assert.assertArrayEquals(TestTokenizer.PATTERN1_TOKENS3, tokens);
  }

  @Test
  public void testTokenizeWithPattern_2_1() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN2,
        TestTokenizer.TEST_VALUE1);
    Assert.assertArrayEquals(TestTokenizer.PATTERN2_TOKENS1, tokens);
  }

  @Test
  public void testTokenizeWithPattern_2_2() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN2,
        TestTokenizer.TEST_VALUE2);
    Assert.assertArrayEquals(TestTokenizer.PATTERN2_TOKENS2, tokens);
  }

  @Test
  public void testTokenizeWithPattern_2_3() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN2,
        TestTokenizer.TEST_VALUE3);
    Assert.assertArrayEquals(TestTokenizer.PATTERN2_TOKENS3, tokens);
  }

  @Test
  public void testTokenizeWithPattern_3_1() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN3,
        TestTokenizer.TEST_VALUE1);
    Assert.assertArrayEquals(TestTokenizer.PATTERN3_TOKENS1, tokens);
  }

  @Test
  public void testTokenizeWithPattern_3_2() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN3,
        TestTokenizer.TEST_VALUE2);
    Assert.assertArrayEquals(TestTokenizer.PATTERN3_TOKENS2, tokens);
  }

  @Test
  public void testTokenizeWithPattern_3_3() {
    final String[] tokens = this.tokenizer.tokenizeWithPattern(TestTokenizer.PATTERN3,
        TestTokenizer.TEST_VALUE3);
    Assert.assertArrayEquals(TestTokenizer.PATTERN3_TOKENS3, tokens);
  }
}
