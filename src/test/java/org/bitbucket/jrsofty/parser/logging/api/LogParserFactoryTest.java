/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

import org.bitbucket.jrsofty.parser.logging.api.LogParserFactory.DefaultLogParserFactory;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatException;
import org.junit.Assert;
import org.junit.Test;

import test.util.LogElementDupAnno;
import test.util.LogElementNoAnnotations;
import test.util.LogElementNoInterface;
import test.util.LogElementOtherAnnotations;
import test.util.LogElementTestObject;
import test.util.LogElementWithParameterConstructor;

public class LogParserFactoryTest {

  private final String intialFormat = "%dtm{dd/MM/yyyy:HH:mm:ss Z} %ip4";
  private final String intialFormatExample = "[30/01/1972:11:42:00 +0200] 192.168.172.200";
  private final String mismatchedFormat = "%dtm{dd/MMM/yyyy:HH:mm:ss Z} %ip4 %msg";

  private final String duplicateFormat = "%url %ip4 %url";
  private final String duplicateFormatExample = "http://www.test1.com 192.168.172.200 http://www.test2.org";

  @Test
  public void testDuplicateTokenParse2()
      throws LogLineFormatException, TokenParseException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.duplicateFormat,
        LogElementDupAnno.class);
    final LogEntry entry = parser.parseLogString(this.duplicateFormatExample);
    final String result = ((LogElementDupAnno) entry).getUrl2();
    Assert.assertTrue("http://www.test2.org".equals(result));
  }

  @Test
  public void testDuplicateTokenParse3()
      throws LogLineFormatException, TokenParseException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.duplicateFormat,
        LogElementDupAnno.class);
    final LogEntry entry = parser.parseLogString(this.duplicateFormatExample);
    final String result = ((LogElementDupAnno) entry).getIp4();
    Assert.assertTrue("192.168.172.200".equals(result));
  }

  @Test
  public void testDuplicateTokenParse1()
      throws LogLineFormatException, TokenParseException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.duplicateFormat,
        LogElementDupAnno.class);
    final LogEntry entry = parser.parseLogString(this.duplicateFormatExample);
    final String result = ((LogElementDupAnno) entry).getUrl1();
    Assert.assertTrue("http://www.test1.com".equals(result));
  }

  @Test(expected = TokenParseException.class)
  public void testFailedParse()
      throws LogLineFormatException, TokenParseException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.intialFormat,
        LogElementTestObject.class);
    parser.parseLogString("[30-01-1972 11:42:00 +0200] 192.168.172.200");
  }

  @Test
  public void testParserParsing()
      throws LogLineFormatException, TokenParseException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.intialFormat,
        LogElementTestObject.class);
    final LogEntry entry = parser.parseLogString(this.intialFormatExample);
    Assert.assertTrue(entry instanceof LogElementTestObject);
  }

  @Test
  public void testParserFactoryInstance() {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    Assert.assertTrue(factory instanceof DefaultLogParserFactory);
  }

  @Test
  public void testDefaultParserInstance()
      throws LogLineFormatException, ClassNotFoundException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.intialFormat,
        Class.forName(LogElementTestObject.class.getName()));

    Assert.assertTrue(parser instanceof DefaultLogParser);

  }

  @Test(expected = IllegalClassException.class)
  public void testMismatch()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(this.mismatchedFormat,
        Class.forName(LogElementTestObject.class.getName()));
  }

  @Test(expected = IllegalClassException.class)
  public void testBadClass()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(this.mismatchedFormat,
        Class.forName(LogElementWithParameterConstructor.class.getName()));
  }

  @Test(expected = IllegalClassException.class)
  public void testNoInterface()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(this.intialFormat,
        Class.forName(LogElementNoInterface.class.getName()));
  }

  @Test(expected = IllegalClassException.class)
  public void testNoAnnotations()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(this.intialFormat,
        Class.forName(LogElementNoAnnotations.class.getName()));
  }

  @Test(expected = IllegalClassException.class)
  public void testNullCatch()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(this.mismatchedFormat,
        null);
  }

  @Test(expected = LogLineFormatException.class)
  public void testNullString()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat(null,
        Class.forName(LogElementOtherAnnotations.class.getName()));
  }

  @Test(expected = LogLineFormatException.class)
  public void testEmptyString()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    factory.createParserForFormat("",
        Class.forName(LogElementOtherAnnotations.class.getName()));
  }

  @Test
  public void testOtherAnnotations()
      throws ClassNotFoundException, LogLineFormatException, IllegalClassException {
    final LogParserFactory factory = LogParserFactory.createDefaultInstance();
    final LogParser parser = factory.createParserForFormat(this.intialFormat,
        Class.forName(LogElementOtherAnnotations.class.getName()));

    Assert.assertTrue(parser instanceof DefaultLogParser);
  }

}
