/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.parser.logging.api;

import org.bitbucket.jrsofty.parser.logging.api.IllegalClassException;
import org.bitbucket.jrsofty.parser.logging.api.LogEntry;
import org.bitbucket.jrsofty.parser.logging.api.LogParser;
import org.bitbucket.jrsofty.parser.logging.api.LogParserFactory;
import org.bitbucket.jrsofty.parser.logging.api.TokenParseException;
import org.bitbucket.jrsofty.parser.logging.util.LogLineFormatException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import test.util.Squid3TestElement;

public class Squid3LogParser {
  private final String squid3TestParse = "192.168.178.26 [15/Jul/2017:15:08:56 +0200] GET \"http://galahtech.stinkbugonline.com/forum/index.php?action=forum\" \"HTTP/1.1\" 200 8600 \"-\" \"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0\" TCP_MISS:HIER_DIRECT";
  private final String squid3Format = "%ip4 %dtm{dd/MMM/yyyy:HH:mm:ss Z} %opt{GET,POST,DELETE,PUT,HEAD,OPTIONS,CONNECT} %url %str %int %int %url %msg %str";
  private LogParser parser;

  @Before
  public void setup() throws LogLineFormatException, IllegalClassException {
    this.parser = LogParserFactory.createDefaultInstance().createParserForFormat(this.squid3Format,
        Squid3TestElement.class);
  }

  @Test
  public void testSquid3Log01() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    Assert.assertTrue(entry instanceof Squid3TestElement);
  }

  @Test
  public void testSquid3Log02() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getIp().equals("192.168.178.26"));
  }

  @Test
  public void testSquid3Log03() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getDateString().equals("15/Jul/2017:15:08:56 +0200"));
  }

  @Test
  public void testSquid3Log04() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getMethod().equals("GET"));

  }

  @Test
  public void testSquid3Log05() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getRequestedUrl()
        .equals("http://galahtech.stinkbugonline.com/forum/index.php?action=forum"));
  }

  @Test
  public void testSquid3Log06() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getHttpType()
        .equals("HTTP/1.1"));
  }

  @Test
  public void testSquid3Log07() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getStatus()
        .equals("200"));
  }

  @Test
  public void testSquid3Log08() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getDataSize()
        .equals("8600"));
  }

  @Test
  public void testSquid3Log09() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getReferringUrl()
        .equals("-"));
  }

  @Test
  public void testSquid3Log10() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getUserAgent()
        .equals("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0"));
  }

  @Test
  public void testSquid3Log11() throws TokenParseException, IllegalClassException {
    final LogEntry entry = this.parser.parseLogString(this.squid3TestParse);
    final Squid3TestElement actual = (Squid3TestElement) entry;
    Assert.assertTrue(actual.getValue1()
        .equals("TCP_MISS:HIER_DIRECT"));
  }

}
